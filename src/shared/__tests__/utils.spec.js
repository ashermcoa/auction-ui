import {updateObject, isEmpty} from "../utils";
describe("updateObject", () => {
    it ("Should correctly update an object properties", () => {
        const bidder = {
            name: "John Doe",
            price: 345.56,
            age: 25
        };

        const expected = {
            name: "John Doe",
            price: 890,
            age: 25
        };

        expect(updateObject(bidder, {price: 890})).toEqual(expected);
    });
});

const emptyString = "";
const nonEmptyString = "Not empty";

const noVariable = undefined;
const noValue = null;

const emptyObj = {};
const nonEmptyObj = {name: "John"};

describe("Testing empty string", () => {
    it("Confirms string is empty", () => {
        expect(isEmpty(emptyString)).toBe(true);
    });
});

describe("Testing non-empty string", () => {
    it("Confirms string is not empty", () => {
        expect(isEmpty(nonEmptyString)).toBe(false);
    });
});

describe("Testing undefined variable", () => {
    it("Confirms the result is empty", () => {
        expect(isEmpty(noVariable)).toBe(true);
    });
});

describe("Testing null value", () => {
    it("Confirms the result is empty", () => {
        expect(isEmpty(noValue)).toBe(true);
    });
});

describe("Testing empty object", () => {
    it("Confirms the result is empty", () => {
        expect(isEmpty(emptyObj)).toBe(true);
    });
});

describe("Testing non-empty object", () => {
    it("Confirms the result is not empty", () => {
        expect(isEmpty(nonEmptyObj)).toBe(false);
    });
});