import * as actions from "../../actions/auction";
import * as types from "../../actions/ActionTypes";

describe("setAuctionItemName", () => {
    it ("Should create an action that add an item name", () => {
        const name = "Rulexgen";

        const expected = {
            type: types.SET_AUCTION_ITEM_NAME,
            name
        };

        expect(actions.setAuctionItemName(name)).toEqual(expected);
    });

    it ("Should fail when name is different", () => {
        const itemName = "Rulexgen";

        const expected = {
            type: types.SET_AUCTION_ITEM_NAME,
            itemName
        };

        expect(actions.setAuctionItemName(name)).not.toEqual(expected);
    });


});

describe("startAuction", () => {
    it ("Should set a startPrice to an auction", () => {
        const amount = 450;

        const expected = {
            type: types.START_AUCTION,
            amount
        };

        expect(actions.startAuction(amount)).toEqual(expected);
    });

    it ("Should fail when given wrong variable name", () => {
        const itemCost = 340;

        const expected = {
            type: types.START_AUCTION,
            itemCost
        };

        expect(actions.startAuction(itemCost)).not.toEqual(expected);
    });
});

describe("updateTimeRemaining", () => {

    it ("Should create an action that updates time remaining", () => {
        const expected = {
            type: types.UPDATE_REMAINING_TIME
        };

        expect(actions.updateTimeRemaining()).toEqual(expected);
    });
});