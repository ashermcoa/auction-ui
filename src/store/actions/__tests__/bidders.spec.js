import * as actions from "../../actions/bidders";
import * as types from "../../actions/ActionTypes";

describe("makeBid", () => {
    it ("Should create a place bid action without fail", () => {
        const name = 1;
        const amount = 400;

        const expected = {
            type: types.MAKE_BID,
            name,
            amount
        };

        expect(actions.makeBid(name, amount)).toEqual(expected);
    });
});

describe("addBidder", () => {
    it ("Should create an action that adds another bidder card.", () => {
        const amount = 450;

        const expected = {
            type: types.ADD_BIDDER_CARD,
            bidder: 450
        };

        expect(actions.addBidder(amount)).toEqual(expected);
    });

});

describe("clearBiddingData", () => {

    it ("Should create an action that clears bidder data from the store.", () => {
        const expected = {
            type: types.CLEAR_BID_DATA
        };

        expect(actions.clearBiddingData()).toEqual(expected);
    });
});