import * as actionTypes  from "./ActionTypes";

export const startAuction = (amount) => {
    return {
        type: actionTypes.START_AUCTION,
        amount
    };
};

export const setAuctionItemName = (name) => {
    return {
        type: actionTypes.SET_AUCTION_ITEM_NAME,
        name
    };
};

export const updateTimeRemaining = (time) => {
    return {
        type: actionTypes.UPDATE_REMAINING_TIME,
        time
    };
};

export const auctionStarted = (started) => {
    return {
        type: actionTypes.AUCTION_STARTED,
        started
    };
};

export const bidCompleted = (completed) => {
    return {
        type: actionTypes.BID_COMPLETED,
        completed
    };
};
export const fetchSessionDataRequest = () => {
    return {
        type: actionTypes.FETCH_AUCTION_SESSION_DATA_REQUEST
    };
};

export const fetchAuctionDataSuccess = (auction) => {
    return {
        type: actionTypes.FETCH_AUCTION_SESSION_DATA_SUCCESS,
        auction
    };
};

export const fetchSessionDataFailure = () => {
    return {
        type: actionTypes.FETCH_AUCTION_SESSION_DATA_FAILURE
    };
};

export const fetchSessionData = () => {
    return {
        type: actionTypes.FETCH_AUCTION_SESSION_DATA
    };
};