import * as actionTypes  from "./ActionTypes";

export const makeBid = (name, amount) => {
    return {
        type: actionTypes.MAKE_BID,
        name,
        amount
    };
};

export const addBidder = (bidder) => {
    return {
        type: actionTypes.ADD_BIDDER_CARD,
        bidder
    };
};

export const clearBiddingData = () => {
    return {
        type: actionTypes.CLEAR_BID_DATA
    };
};

export const fetchBiddersDataSuccess = (bidders) => {
    return {
        type: actionTypes.FETCH_BIDDERS_SESSION_DATA_SUCCESS,
        bidders
    };
};