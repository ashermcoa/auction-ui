import { put } from "redux-saga/effects";

import * as actions from "../actions";

export function* makeBidSaga(action) {
    yield put(actions.makeBid, action.name, action.amount);
}

export function* addBidderSaga() {
    yield put(actions.addBidder);
}

export function* clearBiddingDataSaga() {
    yield put(actions.clearBiddingData);
}