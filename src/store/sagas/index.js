import { takeEvery,all } from "redux-saga/effects";
import * as actionTypes from "../actions/ActionTypes";

import {addBidderSaga, makeBidSaga, clearBiddingDataSaga} from "./bidders";
import {getSessionDataSaga, setAuctionItemNameSaga, startAuctionSaga, updateTimeRemainingSaga} from "./auction";

function* watchBidders() {
    yield takeEvery(actionTypes.MAKE_BID, makeBidSaga);
    yield takeEvery(actionTypes.ADD_BIDDER_CARD, addBidderSaga);
    yield  takeEvery(actionTypes.CLEAR_BID_DATA, clearBiddingDataSaga);
    yield takeEvery(actionTypes.FETCH_AUCTION_SESSION_DATA, getSessionDataSaga);
}

function* watchAuction() {
    yield takeEvery(actionTypes.SET_AUCTION_ITEM_NAME, setAuctionItemNameSaga);
    yield takeEvery(actionTypes.START_AUCTION, startAuctionSaga);
    yield takeEvery(actionTypes.UPDATE_REMAINING_TIME, updateTimeRemainingSaga);
}

export default function* rootSaga() {

    yield all([
        watchAuction(),
        watchBidders()
    ]);
}