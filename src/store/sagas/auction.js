import { put } from "redux-saga/effects";

import * as actions from "../actions";
import {getSessionData} from "../../api";

export function* startAuctionSaga(action) {
    yield put(actions.startAuction, action.amount);
}

export function* setAuctionItemNameSaga(action) {
    yield put(actions.setAuctionItemName, action.name);
}

export function* updateTimeRemainingSaga() {
    yield put(actions.updateTimeRemaining);
}

export function* getSessionDataSaga() {
    yield put(actions.fetchSessionDataRequest());
    try {
        const response = yield getSessionData();
        yield put(actions.fetchBiddersDataSuccess(response.bidders));
        yield put(actions.fetchAuctionDataSuccess(response.auction));
    } catch (error) {
        yield put(actions.fetchSessionDataFailure());
    }
}