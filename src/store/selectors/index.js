export const getMaxBid = (state) => {
    const {auction, bidder} = state;
    const bidders = bidder.bidData;
    const len = bidders.length;

    if (len === 0) {
        return {bidder: "No placed bids", amount: auction.amount};
    }
    let maxBid = bidders[0];
    let maxBidAmount = maxBid.amount;
    for (let i = 0; i < len; i++) {
        const { amount } = bidders[i];
        if (amount > maxBidAmount) {
            maxBid = bidders[i];
        }
    }
    return {bidder: `Bidder ${maxBid.bidder}`, amount: maxBid.amount};
};

export const getHighestBidder = (maxBidder, bidder) => {
    return `Bidder ${bidder}` === maxBidder;
};