import * as actionTypes from "../actions/ActionTypes";
import {updateObject} from "../../shared/utils";

const initialState = {
    bidData: [],
    bidders: [1, 2]
};

const makeBid = (state, action) => {
    const { name, amount } = action;
    return updateObject(state, { bidData: state.bidData.concat({ bidder: name, amount})});
};

const addBidder = (state, action) => {
    return updateObject(state, { bidders: action.bidder});
};

const clearBiddingData = (state) => {
    return updateObject(state,{bidData: []});
};

const populateState = (state, action) => {
    debugger;
    return updateObject(state, action.bidders);
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
    case actionTypes.MAKE_BID: return makeBid(state, action);
    case actionTypes.ADD_BIDDER_CARD: return addBidder(state, action);
    case actionTypes.CLEAR_BID_DATA: return clearBiddingData(state);
    case actionTypes.FETCH_BIDDERS_SESSION_DATA_SUCCESS: return populateState(state, action);

    default: return state;
    }
};

export default reducer;