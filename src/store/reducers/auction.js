import * as actionTypes from "../actions/ActionTypes";
import { updateObject } from "../../shared/utils";

const initialState = {
    itemName: "",
    amount: 0,
    auctionActive: false,
    bidCompleted: false,
    timeLeft: 480
};

const setAuctionItemName = (state, action) => {
    return updateObject(state, { itemName: action.name});
};

const setAuctionPrice = (state, action) => {
    return updateObject(state, { amount: parseInt(action.amount), auctionActive: true, bidCompleted: false });
};

const auctionStarted = (state, action) => {
    return updateObject(state, { auctionActive: action.started});
};

const biddingCompleted = (state, action) => {
    return updateObject(state, { bidCompleted: action.completed, auctionActive: false});
};

const updateTimer = (state, action) => {
    return updateObject(state, { timeLeft: action.time});
};

const populateState = (state, action) => {
    return updateObject(state , action.auction);
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
    case actionTypes.SET_AUCTION_ITEM_NAME: return setAuctionItemName(state, action);
    case actionTypes.START_AUCTION: return setAuctionPrice(state, action);
    case  actionTypes.UPDATE_REMAINING_TIME: return updateTimer(state, action);
    case actionTypes.AUCTION_STARTED: return auctionStarted(state, action);
    case actionTypes.BID_COMPLETED: return biddingCompleted(state, action);
    case actionTypes.FETCH_AUCTION_SESSION_DATA_SUCCESS: return populateState(state, action);
    default: return state;
    }
};

export default reducer;