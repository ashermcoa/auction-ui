import { combineReducers } from "redux";

import auction from "./auction";
import bidders from "./bidders";

const rootReducer = combineReducers({
    bidder: bidders,
    auction: auction
});

export default rootReducer;