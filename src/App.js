//Libraries
import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import socketIOClient from "socket.io-client";

//Styles
import "./App.css";

// Components
import {Button} from "react-bootstrap";
import Auctioneer from "./containers/auctioneer/Auctioneer";
import BidderList from "./containers/bidder/BidderList";

//actions
import * as actions from "./store/actions/index";

//selectors
import {getMaxBid, getHighestBidder} from "./store/selectors";

// Constants
import {SERVER_URL} from "./constants";

class App extends Component {

    constructor() {
        super();
        this.socket = socketIOClient(SERVER_URL);
    }


    getHighestBidder = (bidderNumber) => {
        return getHighestBidder(this.props.maxBidder, bidderNumber);
    };

    outBidden = (bidAmount) => {
        return this.props.maxBid > bidAmount;
    };

    startCounter = () => {
        this.socket.emit("START_AUCTION");
    };

    componentDidMount() {
        this.props.onFetchSessionData();
        this.socket.on("BIDDER_ADDED", data => this.props.addBidder(data));
        this.socket.on("AUCTION_ITEM_ADDED", data => this.props.setItemName(data));
        this.socket.on("AUCTION_ITEM_PRICE_ADDED", data => this.props.setAuctionPrice(data));
        this.socket.on("BID_MADE", data => {
            const { name, amount } = data;
            this.props.makeBid(name, amount);
        });
        this.socket.on("REMAINING_TIME", time => {
            this.props.updateTimeRemaining(time);
        });

        this.socket.on("AUCTION_STARTED", started => {
            this.props.auctionStarted(started);
        });

        this.socket.on("AUCTION_COMPLETED", completed => {
            this.props.biddingCompleted(completed);
            this.socket.emit("REMOVE_TIMERS");
        });
        this.socket.on("BID_DATA_CLEARED", () => {
            this.props.clearBidData();
        });

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    componentDidUpdate(prevProps) {
        if (this.props.bidCompleted !== prevProps.bidCompleted) {
            if (this.props.bidCompleted) {
                clearInterval(this.interval);
            }
        }
    }

    setItemName = (itemName) => {
        this.socket.emit("ITEM_NAME", itemName);
    };

    setAuctionPrice = (price) => {
        this.socket.emit("ITEM_PRICE", price);
    };

    addBidder = () => {
        this.socket.emit("ADD_BIDDER");
    };

    makeBid = (name, amount) => {
        this.socket.emit("MAKE_BID", {name, amount});
    };

    clearBidData = () => {
        this.socket.emit("CLEAR_BID_DATA");
    };

    stopAuction = () => {
        this.props.biddingCompleted(true);
        this.socket.emit("PREEMPT_BIDDING");
    }

    render() {
        return (
            <div className="wrapper">
                <div className="addBidder">
                    <Button onClick={this.addBidder}>Add bidder</Button>
                </div>
                <div className="App">
                    <Auctioneer
                        startCounter={this.startCounter}
                        setItemName={this.setItemName}
                        setAuctionPrice={this.setAuctionPrice}
                        maxBidder={this.props.maxBidder}
                        maxBid={this.props.maxBid}
                        isHighestBidder={false}
                        clearBidData={this.clearBidData}
                        stopAuction={this.stopAuction}
                        preemptBidding={this.stopAuction}
                        remainingTime={this.props.remainingTime}
                        bidCompleted={this.props.bidCompleted}
                        auctionActive={this.props.auctionActive}
                    />
                    <BidderList
                        bidders={this.props.bidders}
                        placeBid={this.makeBid}
                        outBidden={this.outBidden}
                        maxBidder={this.props.maxBidder}
                        maxBid={this.props.maxBid}
                        auctionActive={this.props.auctionActive}
                        remainingTime={this.props.remainingTime}
                        itemName={this.props.itemName}
                        bidCompleted={this.props.bidCompleted}
                        isHighestBidder={this.getHighestBidder}/>
                </div>
            </div>
        );
    }
}

App.propTypes = {
    setItemName: PropTypes.func.isRequired,
    setAuctionPrice: PropTypes.func.isRequired,
    makeBid: PropTypes.func.isRequired,
    updateTimeRemaining: PropTypes.func.isRequired,
    addBidder: PropTypes.func.isRequired,
    clearBidData: PropTypes.func.isRequired,
    auctionStarted: PropTypes.func.isRequired,
    biddingCompleted: PropTypes.func.isRequired,
    onFetchSessionData: PropTypes.func.isRequired,
    itemName: PropTypes.string,
    maxBidder: PropTypes.string,
    maxBid: PropTypes.number,
    remainingTime: PropTypes.number,
    bidders: PropTypes.array.isRequired,
    auctionActive: PropTypes.bool.isRequired,
    bidCompleted: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
    return {
        bidders: state.bidder.bidders,
        maxBid: getMaxBid(state).amount,
        maxBidder: getMaxBid(state).bidder,
        itemName: state.auction.itemName,
        auctionActive: state.auction.auctionActive,
        remainingTime: state.auction.timeLeft,
        bidCompleted: state.auction.bidCompleted
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setItemName: (name) => dispatch(actions.setAuctionItemName(name)),
        setAuctionPrice: (price) => dispatch(actions.startAuction(price)),
        makeBid: (bidder, amount) => dispatch(actions.makeBid(bidder, amount)),
        addBidder: (data) => dispatch(actions.addBidder(data)),
        updateTimeRemaining: (time) => dispatch(actions.updateTimeRemaining(time)),
        clearBidData: () => dispatch(actions.clearBiddingData()),
        auctionStarted: (started) => dispatch(actions.auctionStarted(started)),
        biddingCompleted: (completed) => dispatch(actions.bidCompleted(completed)),
        onFetchSessionData: () => dispatch(actions.fetchSessionData())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
