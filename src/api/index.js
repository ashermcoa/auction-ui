import {SERVER_URL} from "../constants";

export const getSessionData = () => {
    return fetch(`${SERVER_URL}/api`)
        .then(res => res.json());
};
