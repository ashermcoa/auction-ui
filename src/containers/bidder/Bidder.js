import React from "react";
import PropTypes from "prop-types";
//Components
import HeaderComponents from "../../components/bidder/HeaderComponents";
import BidInfo from "../../components/bidder/BidInfo";

//Styles
import "./Bidder.css";


const Bidder = (props) =>
    (
        <div className="bidderTab">
            <div className="bidderTabHeader">
                <HeaderComponents.Participant entry={props.entry} />
                { props.auctionActive && <HeaderComponents.CountDownTitle remainingTime={props.remainingTime} /> }
            </div>
            <BidInfo
                auctionActive={props.auctionActive}
                itemName={props.itemName}
                isHighestBidder={props.isHighestBidder}
                maxBid={props.maxBid}
                maxBidder={props.maxBidder}
                entry={props.entry}
                outBidden={props.outBidden}
                remainingTime={props.remainingTime}
                bidCompleted={props.bidCompleted}
                placeBid={(bidder, amount) => props.placeBid(bidder, amount)}
            />
        </div>
    );

Bidder.propTypes = {
    auctionActive: PropTypes.bool,
    itemName: PropTypes.string,
    isHighestBidder: PropTypes.bool,
    maxBidder: PropTypes.string,
    maxBid: PropTypes.number,
    entry: PropTypes.number.isRequired,
    remainingTime: PropTypes.number,
    outBidden:  PropTypes.func.isRequired,
    placeBid: PropTypes.func.isRequired,
    bidCompleted: PropTypes.bool.isRequired
};

export default Bidder;