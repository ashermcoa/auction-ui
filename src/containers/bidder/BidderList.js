import React from "react";
import PropTypes from "prop-types";

import Bidder from "./Bidder";

const BidderList = (props) => (
    <div className="biddersWrapper">
        { props.bidders.map(bidder => (
            <Bidder
                key={bidder}
                entry={bidder}
                outBidden={props.outBidden}
                isHighestBidder={props.isHighestBidder(bidder)}
                placeBid={props.placeBid}
                itemName={props.itemName}
                maxBid={props.maxBid}
                maxBidder={props.maxBidder}
                auctionActive={props.auctionActive}
                bidCompleted={props.bidCompleted}
                remainingTime={props.remainingTime}
            />
        )) }
    </div>
);

BidderList.propTypes = {
    bidders: PropTypes.array.isRequired,
    isHighestBidder: PropTypes.func.isRequired,
    placeBid: PropTypes.func.isRequired,
    outBidden: PropTypes.func.isRequired,
    auctionActive: PropTypes.bool,
    itemName: PropTypes.string,
    maxBidder: PropTypes.string,
    maxBid: PropTypes.number,
    remainingTime: PropTypes.number,
    bidCompleted: PropTypes.bool.isRequired
};

export default BidderList;