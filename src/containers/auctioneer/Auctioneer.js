import React from "react";
import PropTypes from "prop-types";

import { Button } from "react-bootstrap";
import AuctionForm from "../../components/auctioneer/AuctionForm";

//Components
import BidRank from "../../components/shared/BidRank";
import BidSummary from "../../components/shared/BidSummary";

const Auctioneer  = (props) => (
    <div className="auctionTab">
        <div>
            <h4>Auctioneer</h4> </div>
        { props.auctionActive && <Button  onClick={props.preemptBidding} bsStyle="danger">Stop bidding</Button> }
        <AuctionForm
            startCounter={props.startCounter}
            setItemName={props.setItemName}
            clearBidData={props.clearBidData}
            bidCommpleted={props.bidCompleted}
            remainingTime={props.remainingTime}
            bidCompleted={props.bidCompleted}
            stopAuction={props.stopAuction}
            auctionActive={props.auctionActive}
            setPrice={props.setAuctionPrice}/>
        {!props.bidCompleted &&
            <BidRank
                isHighestBidder={props.isHighestBidder}
                maxBid={props.maxBid}
                remainingTime={props.remainingTime}
                maxBidder={props.maxBidder}
            />
        }
        {props.bidCompleted && <BidSummary
            maxBidder={props.maxBidder}
            maxBid={props.maxBid}
        />}
    </div>
);

Auctioneer.propTypes = {
    setItemName: PropTypes.func.isRequired,
    setAuctionPrice: PropTypes.func.isRequired,
    startCounter: PropTypes.func.isRequired,
    clearBidData: PropTypes.func.isRequired,
    isHighestBidder: PropTypes.bool.isRequired,
    remainingTime: PropTypes.number.isRequired,
    preemptBidding: PropTypes.func.isRequired,
    maxBid: PropTypes.number,
    maxBidder: PropTypes.string,
    bidCompleted: PropTypes.bool.isRequired,
    auctionActive: PropTypes.bool.isRequired,
};

export default Auctioneer;