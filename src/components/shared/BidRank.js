import React from "react";
import PropTypes from "prop-types";

const BidRank = ({isHighestBidder, maxBid, maxBidder}) => (
    <div>
        { isHighestBidder && <p>
            You are the highest bidder at $ {maxBid}
        </p>}
        { !isHighestBidder && <div>
            <h5>Current price: </h5>
            <p>${maxBid} - {maxBidder}</p>
        </div>}
    </div>
);

BidRank.propTypes = {
    isHighestBidder: PropTypes.bool.isRequired,
    maxBid: PropTypes.number,
    maxBidder: PropTypes.string
};

export default BidRank;