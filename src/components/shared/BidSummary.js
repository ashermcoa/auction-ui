import React from "react";
import PropTypes from "prop-types";

const BidSummary = ({maxBid, maxBidder}) => (
    <div>
        <h5>Most recent auction </h5>
        <p>Winner: {maxBidder}</p>
        <p>Winning price: ${maxBid}</p>
    </div>
);

BidSummary.propTypes = {
    maxBid: PropTypes.number,
    maxBidder: PropTypes.string
};

export default BidSummary;