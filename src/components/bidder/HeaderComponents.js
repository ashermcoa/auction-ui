import React from "react";
import PropTypes from "prop-types";

//styles
import styles from "./HeaderComponents.css";


const HeaderComponents = {
    Participant: function Participant(props) {
        return <h4 className={styles.bidderHeader}>Bidder {props.entry}</h4>;
    },

    CountDownTitle: function CountDownTitle(props) {
        return <h5>Time left {props.remainingTime}</h5>;
    }
};

HeaderComponents.Participant.propTypes = {
    entry: PropTypes.number
};

HeaderComponents.CountDownTitle.propTypes = {
    remainingTime: PropTypes.number
};

export default HeaderComponents;