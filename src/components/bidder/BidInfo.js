import React, { Component } from "react";
import PropTypes from "prop-types";

//Components
import BidRank from "../shared/BidRank";
import BidPlacer from "./BidPlacer";
import BidSummary from "../shared/BidSummary";

class BidInfo extends Component {
    render() {
        const { itemName, auctionActive, isHighestBidder, maxBid, maxBidder, bidCompleted } = this.props;
        return (
            <div>
                {auctionActive && <div>
                    <h4>Item name: {itemName}</h4>
                    <BidRank isHighestBidder={isHighestBidder} maxBid={maxBid} maxBidder={maxBidder} />
                    <BidPlacer
                        placeBid={this.props.placeBid}
                        entry={this.props.entry}
                        maxBid={this.props.maxBid}
                        outBidden={this.props.outBidden}
                    />
                </div>}
                {bidCompleted && <div>
                    {isHighestBidder && <h3>Congratulations! You just won!</h3>}
                    <BidSummary
                        maxBid={maxBid}
                        maxBidder={maxBidder}
                    />
                </div>}
                {!auctionActive && !bidCompleted && <div>
                    <p>There is no active auction ongoing</p>
                    <p>Please wait until one becomes available</p>
                </div>}
            </div>
        );
    }
}

BidInfo.propTypes = {
    placeBid: PropTypes.func.isRequired,
    itemName: PropTypes.string,
    auctionActive: PropTypes.bool.isRequired,
    isHighestBidder: PropTypes.bool,
    maxBid: PropTypes.number,
    maxBidder: PropTypes.string,
    entry: PropTypes.number.isRequired,
    outBidden: PropTypes.func.isRequired,
    bidCompleted: PropTypes.bool.isRequired
};

export default BidInfo;