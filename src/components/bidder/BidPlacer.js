import React, { Component } from "react";
import PropTypes from "prop-types";
import { FormGroup, ControlLabel ,Button, FormControl, HelpBlock } from "react-bootstrap";

class BidPlacer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            bidAmount: "" ,
            hasBid: false,
            showError: false
        };
    }

    onBidAmountChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    }

    onPlaceBid = () => {
        if (this.state.bidAmount < this.props.maxBid) {
            this.setState({showError: true});
        } else {
            this.props.placeBid(this.props.entry, isNaN(this.state.bidAmount) ? 0 : parseInt(this.state.bidAmount));
            this.setState({hasBid: true, showError: false});
        }

    }

    outBidden = () => {
        // Only show the "Bid again" message if this bidder has already placed one.
        return this.props.outBidden(this.state.bidAmount) && this.state.hasBid;
    }
    
    render() {
        return (
            <FormGroup
                controlId="formBasicText"
            >
                <ControlLabel>Enter your bid amount</ControlLabel>
                { this.outBidden() && !this.state.showError && <HelpBlock>Bid again.</HelpBlock> }
                { this.state.showError && <HelpBlock>Your bid is lower than the current price! Please bid again.</HelpBlock> }
                <FormControl
                    type="text"
                    name="bidAmount"
                    onChange={this.onBidAmountChange}
                    value={this.state.bidAmount}
                    placeholder="Enter bid amount"
                />
                <Button className="btn-placeBid" onClick={this.onPlaceBid} bsStyle="primary">Place bid</Button>
            </FormGroup>
        );
    }
}

BidPlacer.propTypes = {
    placeBid: PropTypes.func.isRequired,
    entry: PropTypes.number.isRequired,
    outBidden: PropTypes.func.isRequired,
    maxBid: PropTypes.number.isRequired
};

export default BidPlacer;