import React, { Component } from "react";
import PropTypes from "prop-types";

import { isEmpty} from "../../shared/utils";

import {FormControl, FormGroup, ControlLabel} from "react-bootstrap";

class AuctionForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            fields: {
                itemName: "",
                startPrice: 0
            }
        };
        this.priceRef = React.createRef();
        this.nameRef = React.createRef();
    }

    componentDidMount() {
        this.nameRef.current.focus();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.bidCompleted !== this.props.bidCompleted) {
            if (this.props.bidCompleted) {
                const fields = this.state.fields;
                fields.itemName = "";
                fields.startPrice = 0;
                this.setState({fields});

                // Focus on the item name text field when auction is completed.
                this.nameRef.current.focus();

            }
        }
    }

    onInputChange = (evt) => {
        const fields = this.state.fields;
        const value = evt.target.value;
        const name = evt.target.name;
        fields[name] = value;
        this.setState({ fields });
    }

    submit = () => {
        // Only start when values are valid and auction is not active.
        if (this.isValid() && !this.props.auctionActive) {
            this.props.startCounter();
            this.props.setItemName(this.state.fields.itemName);
            this.props.setPrice(this.state.fields.startPrice);

            // Clear previous auction data before next auction.
            if (this.props.bidCompleted) {
                this.props.clearBidData();
            }
        }
    }

    handleBlur = () => {
        this.submit();
    }

    handleKeyPress = (e) => {
        // Enter key on keyboard
        if (e.key === "Enter") {
            this.submit();
        }
    }




    isValid = () => {
        return !isEmpty(this.state.fields.itemName) && this.state.fields.startPrice > 0;
    }

    render() {
        return (
            <div>
                <form>
                    <FormGroup
                        controlId="formBasicText"
                    >
                        <ControlLabel>Item name</ControlLabel>
                        <FormControl
                            type="text"
                            name="itemName"
                            value={this.state.fields.itemName}
                            placeholder="Enter item name"
                            inputRef={this.nameRef}
                            onBlur={this.handleBlur}
                            onChange={this.onInputChange} />
                        <ControlLabel>starting price</ControlLabel>
                        <FormControl
                            type="number"
                            name="startPrice"
                            value={this.state.fields.startPrice}
                            placeholder="0"
                            onKeyPress={this.handleKeyPress}
                            onBlur={this.handleBlur}
                            onChange={this.onInputChange}
                        />
                    </FormGroup>
                </form>
            </div>
        );
    }

}

AuctionForm.propTypes = {
    setPrice: PropTypes.func.isRequired,
    setItemName: PropTypes.func.isRequired,
    startCounter: PropTypes.func.isRequired,
    clearBidData: PropTypes.func.isRequired,
    bidCompleted: PropTypes.bool.isRequired,
    auctionActive: PropTypes.bool.isRequired,
};

export default AuctionForm;