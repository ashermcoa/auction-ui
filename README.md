# Real-time bidding platform
A stub version of a real-time bidding platform

# Installation

1. Unzip the project and ``cd <project-dir>``
2. Use `npm install` or `yarn` to install the dependencies.
3. Run `npm start` to launch the app. This should open your browser. If not, open http://localhost:3000.
4. Run `npm run test` to run tests.

## Links:

[Backend repository](https://github.com/ashermcoa/auc-srv) 

[Live demo](https://bw-auctionapp.herokuapp.com/)

## Libraries used
 - React.js
 - Redux
 - Redux-saga
 
 ## Notes:
 > Specify the backend web socket server in `${CLIENT_PROJECT_DIR}/auction-ui/src/constants/index.js`
 
 > Each bidding session had been given a timed duration of `180 seconds`.
 To edit, change the `maxBiddingTime` value in the backend project folder, please check `${BACKEND_PROJECT_DIR}//config/default.json`
 
 
 > The application data can survive a browser reload but not a server restart.
 
 > After the auctioneer has entered the item name and price, the bidders screen activates 
 and becomes enabled for bidders to place bids.
 
 > At the end of each bidding session, the bidders' screens and the auction display a brief summary of the 
 just ended auction, listing the bid winner and the price they had placed.
 
 ## Todo
 - Write more tests.
 - Persist values to a database (preferrably a NoSQL database)
